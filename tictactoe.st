"
Brad Whitfield
HW4 - CS451
Dr. Il-Hyung Cho

This is version 1 of the TicTacToe game, meaning the AI is dumb and just picks a move at random.

This program is a basic TicTacToe game based in GNU SmallTalk. It's a single player game in which you play a computer.
The computer is X, while you are O. The first time around, the user (you) will go first. The second game the computer will go first.
This will alternate every other game. A scoreboard will keep track of all the wins and tied games.
"

Object subclass: Board [
  | positions stdOutput possibleWins remainingCount |
  <comment: 'This is the board for a tictactoe game. The board knows how to print itself, the moves of the user and computer, and can determine a winner.'>
  
  Board class >> new [
    <category: 'instance creation'>
    | b |
    b := super new.
    b init.
    ^b
  ]
  
  "Creates a dictionary containing the current states of each move. It probably shouldn't have been a dictionary,
   but this languages is picky, so I'm afraid to touch anything. The lazy list of checking for wins is also initialized
   in here, but again, I'm too afraid to try to do it a better way."
  init [
    <category: 'initialization'>
    positions := Dictionary new.
    positions at: 1 put: '1'; at: 2 put: '2'; at: 3 put: '3'; at: 4 put: '4';
      at: 5 put: '5'; at: 6 put: '6'; at: 7 put: '7'; at: 8 put: '8'; at: 9 put: '9'.
    possibleWins := #((1 2 3) (4 5 6) (7 8 9) (1 4 7) (2 5 8) (3 6 9) (1 5 9) (3 5 7)).
    remainingCount := 9.
    stdOutput := Transcript
  ]
  
  "The next two functions make printing crap a lot easier. I found this, or similiar in several resources online."
  stdOutput: output [
    <category: 'print output'>
    ^stdOutput
  ]

  stdOutput: output [
    <category: 'print output'>
    stdOutput := output
  ]
  
  "Prints the game board in it's current state."
  printOn: stream [
    stdOutput cr;
      show: '----------';
      cr;
      show: '| ', (positions at: 1), '| ', (positions at: 2), '| ', (positions at: 3), '|';
      cr;
      show: '----------';
      cr;
      show: '| ', (positions at: 4), '| ', (positions at: 5), '| ', (positions at: 6), '|';
      cr;
      show: '----------';
      cr;
      show: '| ', (positions at: 7), '| ', (positions at: 8), '| ', (positions at: 9), '|';
      cr;
      show: '----------';
      cr
  ]
  
  "Returns true if the value can be set, and false if the move is already taken."
  setX: position [
    <category: 'Set move'>
    (((positions at: position) = 'X') | ((positions at: position) = 'O'))
      ifTrue: [
        "Illegal move!"
        ^false
      ]
      ifFalse: [
        "Change value."
        positions at: position put: 'X'.
        remainingCount := remainingCount - 1.
        ^true
      ]
  ]
  
  "Returns true if the value can be set, and false if the move is already taken."
  setO: position [
    <category: 'Set move'>
    (((positions at: position) = 'X') | ((positions at: position) = 'O'))
      ifTrue: [
        "Illegal move!"
        ^false
      ]
      ifFalse: [
        "Change the move to 'O'."
        positions at: position put: 'O'.
        remainingCount := remainingCount - 1.
        ^true
      ]
  ]
  
  "Check for a win."
  checkForWin [
    | first second third | 
    possibleWins do: [:path |
      "Get the values at the possible winning path."
      first := positions at: (path at: 1).
      second := positions at: (path at: 2).
      third := positions at: (path at: 3).
      "If the first value is 'X' or 'O', then check if all three are the same."
      (((first = 'X') | (first = 'O')) & ((first = second) & (first = third)))
        ifTrue: [
          ^first
        ]
    ].
    "No win found, so just return nil."
    ^nil
  ]
 
  "Get all possible moves remaining so the computer can randomly pick." 
  getRemainingChoices [
    <category: 'possible moves'>
    | remaining keys index |
    "The keys are equivalent to the position numbers on the board."
    keys := positions keys.
    "This array will hold the remaining move keys for the dictionary."
    remaining := Array new: remainingCount.
    index := 1.
    "Iterate through all of the keys and get the ones not taken already."
    keys do: [:p |
      (((positions at: p) = 'X') | ((positions at: p) = 'O'))
        ifFalse: [
          remaining at: index put: p.
          index := index + 1.
        ].
    ].
    ^remaining
  ]
  
  "informs the user if there are any possible moves left."
  movesRemain [
    <category: 'possible moves'>
    (remainingCount > 0)
      ifTrue: [^true.]
      ifFalse: [^false.]
  ]
  
].

Object subclass: Score [
  <comment: 'This is the class that keeps track of the current scores and prints them for the user.'>
  | userScore computerScore stdOutput tiedGames |
  
  Score class >> new [
    <category: 'instance creation'>
    | s |
    s := super new.
    s init.
    ^s
  ]
  
  "Sets each score to 0 along with the Transcript object for printing."
  init [
    <category: 'initialization'>
    userScore := 0.
    computerScore := 0.
    tiedGames := 0.
    stdOutput := Transcript
  ]
 
  "What you think it does." 
  giveUserPoint [
    <category: 'give points'>
    userScore := userScore + 1.
  ]
  
  "What you think it does."
  giveComputerPoint [
    <category: 'give points'>
    computerScore := computerScore + 1
  ]
  
  "Increments the count for the number of tied games."
  tieGamePoint [
    <category: 'give points'>
    tiedGames := tiedGames +1
  ]
   
  "The next two functions make printing crap a lot easier."
  stdOutput: output [
    <category: 'print output'>
    ^stdOutput
  ]

  stdOutput: output [
    <category: 'print output'>
    stdOutput := output
  ]
  
  "Prints the scores for the user and computer."
  printOn: stream [
    stdOutput cr;
      show: '*** Score Board ***';
      cr;
      show: 'User Wins: ', (userScore printString);
      cr;
      show: 'Computer Wins: ', (computerScore printString);
      cr;
      show: 'Tie Games: ', (tiedGames printString);
      cr
  ]
].

"This is where the game actually begins."

"Initialize the score, gameboard, turn stuff, and some other less intersting stuff."
scoreBoard := Score new.
gameBoard := Board new.

userFirst := true.
userTurn := true.
keepPlaying := true.

movePicker := Random new.
stdOut := Transcript.

"Play the game till the user decides otherwise."
[keepPlaying]
whileTrue: [

  "Run till someone has won, or the board is full."
  [((gameBoard checkForWin) = nil) & (gameBoard movesRemain)]
  whileTrue: [
    | input remaining |
    (userTurn)
      ifTrue: [
        "Users turn, so have them pick a number and try to set it as a move."
        gameBoard printNl.
        stdOut show: 'pick a move: '.
        input := stdin nextLine asInteger.
        
        (gameBoard setO: input)
          ifFalse: [
            stdOut show: 'That is not a valid move. Please select another.'; cr.
          ]
          ifTrue: [
            userTurn := false.
          ]
      ]
      ifFalse: [
        "Computers turn. Get the remaining set and pick one at random."
        remaining := gameBoard getRemainingChoices.
        position := movePicker between: 1 and: (remaining size).
        gameBoard setX: (remaining at: position).
        userTurn := true.
      ]
  ].

  
  "Determine who one, if anyone."
  ((gameBoard checkForWin) = 'O')
    ifTrue: [
      scoreBoard giveUserPoint.
      stdOut show: 'Good job! You won!'; cr.
    ].

  ((gameBoard checkForWin) = 'X')
    ifTrue: [
      scoreBoard giveComputerPoint.
      stdOut show: 'You were beat by a computer!'; cr.
    ].

  ((gameBoard checkForWin) = nil)
    ifTrue: [
      scoreBoard tieGamePoint.
      stdOut show: 'Tie game!'; cr.
    ].
  
  "Print out the final game board and scores."
  gameBoard printNl.
  scoreBoard printNl.

  "Flip so the compuer goes first, if the user went first, or the other way around."
  userFirst := userFirst not.
  userTurn := userFirst.

  "Prompt the user to see if they want to play again."
  stdOut show: 'Would you like to play another game? (Y/N) '.
  choice := stdin nextLine.

  "Only do anything if they picked 'y' or 'Y'."
  ((choice = 'y') | (choice = 'Y'))
    ifTrue: [gameBoard := Board new.]
    ifFalse: [keepPlaying := false.]
]
